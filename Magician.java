public class Magician extends Player{
  
    public Magician(Player player){
        super(player.getName());
        setJob("Magician",120,player.getLevel(),30,player.bag);
    }
}