public class Player{
    
    private String  Name ;
    private String job;
    private int maxHp;
    private int  myExp ;
    private int Level ;
    private int myHp ;
    private int myDamage ;
    Bag bag = new Bag();

    public Player(String name){
        Name = name;
        maxHp = 100;
        myExp = 0;
        Level = 1;
        myHp = maxHp;
        job = "Novice";
        myDamage = 20;
    }
	public void setJob(String Job,int Hp,int level,int damage,Bag _bag){
         myHp = Hp;
         maxHp = Hp;
         level = Level;
         myDamage = damage;
         bag = _bag;
         job = Job;
    }

    public void setName(String playerName){
        Name = playerName ;
   }

   public void setHp(int Hp){
       if(Hp>100){
           myHp = 100;
       }
       else{
           myHp = Hp;
       }
    }
    public void setExp(int Exp){
        myExp = Exp;
        if(myExp>=100){
            Level++;
            myExp-=100;
        }
    }
    public int usePosion(int index){
        if(index > bag.Items.size() || index <= 0){
            System.out.println("\n No Item you choose please choose again \n");
            return 1;
        }
        else{
        myHp = myHp+bag.Items.get(index-1).getHeal();
        bag.useHealItem(index);
            return 0;
        }
    }
   
   public void ShowDetailPlayer(){
    System.out.println("================================");
    System.out.println("  Name : "+ Name);
    System.out.println("  Hp : "+ myHp);
    System.out.println("  Exp : "+ myExp);
    System.out.println("  Level : "+ Level);
    System.out.println("  Class : "+ job);
    System.out.println("  Damage : "+ myDamage);
    System.out.println("================================\n");
   }

   
   public int getDamage(){
       return myDamage;
   }
   public int getHp(){
        return myHp;
   }
   public int getExp(){
       return myExp;
   }
   public String getName(){
       return Name;
   }
   public int getLevel(){
       return Level;
   }
   public int getHpMax(){
       return maxHp;

   }
}