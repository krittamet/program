
public class Potion extends Item{
    private String detailItem;

    public Potion(){
        setValue("Potion",25);
        detailItem = "+Hp 25";
    }
    public void print(){
        System.out.println("Potion  "+detailItem);
    }
    
}