import java.util.*;

public class Bag{


    private int Capacity;
    ArrayList<Item> Items ;

    public Bag(){
        Capacity = 10;
        Items = new ArrayList<Item>(Capacity);  
    }
    public void showItem(){
        System.out.println("++++++++++In Bag++++++++");
        int i=1;
        for(Item showItem : Items) {
        System.out.print(i+". ");
        showItem.print();
        i++;
        }
        if(Items.size()==0){
            System.out.println("No Item ");
        }
    }
    public void addPotion(String itemName){
        itemName = itemName.toLowerCase();
        if(checkFull()){
        if(itemName.equals("potion")){
            Items.add(new Potion());
        }
        else if(itemName.equals("superpotion")){
            Items.add(new Superpotion());
        }
    }
        else{
            System.out.println("Full Bag");
        }
        
    }
    public boolean checkFull(){
        if(Items.size()==10){
            return false;
        }
        return true;
    }
    public void useHealItem(int index){
        Items.remove(index-1);
    }
}